﻿namespace TextFileRead1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()                                       // Changed to public
        {
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.Main = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBoxDEBUG = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnSelectFile_click = new System.Windows.Forms.Button();
            this.richTextBoxOut1 = new System.Windows.Forms.RichTextBox();
            this.Advanced = new System.Windows.Forms.TabPage();
            this.About = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.statusText = new System.Windows.Forms.Label();
            this.button_TEST = new System.Windows.Forms.Button();
            this.TabControl1.SuspendLayout();
            this.Main.SuspendLayout();
            this.About.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.Main);
            this.TabControl1.Controls.Add(this.Advanced);
            this.TabControl1.Controls.Add(this.About);
            this.TabControl1.Location = new System.Drawing.Point(2, 2);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(1104, 620);
            this.TabControl1.TabIndex = 28;
            // 
            // Main
            // 
            this.Main.Controls.Add(this.button_TEST);
            this.Main.Controls.Add(this.label1);
            this.Main.Controls.Add(this.richTextBoxDEBUG);
            this.Main.Controls.Add(this.richTextBox2);
            this.Main.Controls.Add(this.richTextBox1);
            this.Main.Controls.Add(this.btnSelectFile_click);
            this.Main.Controls.Add(this.richTextBoxOut1);
            this.Main.Location = new System.Drawing.Point(4, 22);
            this.Main.Name = "Main";
            this.Main.Padding = new System.Windows.Forms.Padding(3);
            this.Main.Size = new System.Drawing.Size(1096, 594);
            this.Main.TabIndex = 0;
            this.Main.Text = "Main";
            this.Main.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(398, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // richTextBoxDEBUG
            // 
            this.richTextBoxDEBUG.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.richTextBoxDEBUG.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBoxDEBUG.ForeColor = System.Drawing.SystemColors.Window;
            this.richTextBoxDEBUG.Location = new System.Drawing.Point(609, 6);
            this.richTextBoxDEBUG.Name = "richTextBoxDEBUG";
            this.richTextBoxDEBUG.ReadOnly = true;
            this.richTextBoxDEBUG.Size = new System.Drawing.Size(206, 581);
            this.richTextBoxDEBUG.TabIndex = 34;
            this.richTextBoxDEBUG.Text = "";
            this.richTextBoxDEBUG.WordWrap = false;
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox2.ForeColor = System.Drawing.SystemColors.Window;
            this.richTextBox2.Location = new System.Drawing.Point(6, 237);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(523, 172);
            this.richTextBox2.TabIndex = 33;
            this.richTextBox2.Text = "";
            this.richTextBox2.WordWrap = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.richTextBox1.Location = new System.Drawing.Point(6, 415);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(523, 172);
            this.richTextBox1.TabIndex = 32;
            this.richTextBox1.Text = "";
            this.richTextBox1.WordWrap = false;
            // 
            // btnSelectFile_click
            // 
            this.btnSelectFile_click.Location = new System.Drawing.Point(392, 62);
            this.btnSelectFile_click.Name = "btnSelectFile_click";
            this.btnSelectFile_click.Size = new System.Drawing.Size(211, 110);
            this.btnSelectFile_click.TabIndex = 31;
            this.btnSelectFile_click.Text = "Select File";
            this.btnSelectFile_click.UseVisualStyleBackColor = true;
            this.btnSelectFile_click.Click += new System.EventHandler(this.btnSelectFile_click_Click);
            // 
            // richTextBoxOut1
            // 
            this.richTextBoxOut1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.richTextBoxOut1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBoxOut1.Location = new System.Drawing.Point(821, 6);
            this.richTextBoxOut1.Name = "richTextBoxOut1";
            this.richTextBoxOut1.Size = new System.Drawing.Size(269, 581);
            this.richTextBoxOut1.TabIndex = 30;
            this.richTextBoxOut1.Text = "";
            this.richTextBoxOut1.TextChanged += new System.EventHandler(this.richTextBoxOut1_TextChanged_1);
            // 
            // Advanced
            // 
            this.Advanced.Location = new System.Drawing.Point(4, 22);
            this.Advanced.Name = "Advanced";
            this.Advanced.Padding = new System.Windows.Forms.Padding(3);
            this.Advanced.Size = new System.Drawing.Size(1096, 594);
            this.Advanced.TabIndex = 1;
            this.Advanced.Text = "Advanced";
            this.Advanced.UseVisualStyleBackColor = true;
            // 
            // About
            // 
            this.About.Controls.Add(this.listBox1);
            this.About.Location = new System.Drawing.Point(4, 22);
            this.About.Name = "About";
            this.About.Padding = new System.Windows.Forms.Padding(3);
            this.About.Size = new System.Drawing.Size(1096, 594);
            this.About.TabIndex = 2;
            this.About.Text = "About";
            this.About.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "H2T Converter",
            "Version:",
            "Created date: ",
            "Created by: Matija Bensa"});
            this.listBox1.Location = new System.Drawing.Point(102, 82);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(297, 238);
            this.listBox1.TabIndex = 0;
            // 
            // statusText
            // 
            this.statusText.AutoSize = true;
            this.statusText.Location = new System.Drawing.Point(6, 625);
            this.statusText.Name = "statusText";
            this.statusText.Size = new System.Drawing.Size(35, 13);
            this.statusText.TabIndex = 29;
            this.statusText.Text = "status";
            // 
            // button_TEST
            // 
            this.button_TEST.Location = new System.Drawing.Point(257, 62);
            this.button_TEST.Name = "button_TEST";
            this.button_TEST.Size = new System.Drawing.Size(75, 23);
            this.button_TEST.TabIndex = 36;
            this.button_TEST.Text = "Save";
            this.button_TEST.UseVisualStyleBackColor = true;
            this.button_TEST.Click += new System.EventHandler(this.button_TEST_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1110, 647);
            this.Controls.Add(this.statusText);
            this.Controls.Add(this.TabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "H2T Converter";
            this.TabControl1.ResumeLayout(false);
            this.Main.ResumeLayout(false);
            this.Main.PerformLayout();
            this.About.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage Main;
        private System.Windows.Forms.TabPage Advanced;
        private System.Windows.Forms.TabPage About;
        private System.Windows.Forms.Label statusText;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnSelectFile_click;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBoxDEBUG;
        private System.Windows.Forms.RichTextBox richTextBoxOut1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_TEST;
    }
}

