﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using System.IO;

using Microsoft.Office.Interop.Excel;
using TextFileRead1.Scripts;

//using System.Data;

namespace TextFileRead1
{
    public partial class Form1 : Form
    {
        private System.Windows.Forms.Label label;

        //public MainProgram mainProgram = new MainProgram();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSelectFile_click_Click(object sender, EventArgs e)      ///***************************************************************************************************
        {
            Phase1();
        }

        public void Phase1()
        {
            MainProgram mainProgram = new MainProgram();
            //mainProgram.onBottonClick();
            // []. Select File:
            ClearDebugWindows();
            mainProgram.SelectFile();
            // []. Read file and make a list of every line:
            mainProgram.ReadWholeFile();
            // []. Copy the list to the textbox(raw data):
            mainProgram.CopyListToTextBox_A(richTextBox1);
            // []. Filter every line:
            mainProgram.SearchForMatches(richTextBoxDEBUG, richTextBoxOut1);
            //mainProgram.ConvertGableFrame();
            // []. Create list of filtered data:

            // []. Copy list of filtered data to textbox:

            // []. Save to file:
            //SaveToFile();
            
            //TEST:
            //FilterID_1sArrayListProgram FilterID_1sArrayListProgram = new FilterID_1sArrayListProgram();
            //FilterID_1sArrayListProgram.GoThroughIDs();

            //mainProgram.MyArray();
            //mainProgram.Test1(); 
        }
        
        public void ClearDebugWindows()
        {
            richTextBox1.Clear();
            richTextBox2.Clear();
            richTextBoxDEBUG.Clear();
            richTextBoxOut1.Clear();
        }

        public void SaveToFile()
        {
            MainProgram mainProgram = new MainProgram();

            var filename_postfix = "Frame";
            var filename1 = mainProgram.halberFrameFile_path;
            var filename = filename1 + filename_postfix;

            filename = "Frame_Test";

            string DSGableFrame_Line1 = "DSGableFrame_attributes.saveas_file \"Gable References\"\n";
            string DSGableFrame_Line2 = "DSGableFrame_attributes.get_menu \"standard\"\n";
            string DSGableFrame_Line3 = "DSGableFrame_attributes.name_enable 1\n";

            string DSGableFrame_LinesCombined = DSGableFrame_Line1 + DSGableFrame_Line2 + DSGableFrame_Line3;


            string FinalOutputText = DSGableFrame_LinesCombined + richTextBoxOut1.Text;

            System.IO.File.WriteAllText(@"C:\Users\Matija\Desktop\" + filename + ".p_DSGableFrame", FinalOutputText);


        }

        private void richTextBoxOut1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button_TEST_Click(object sender, EventArgs e)
        {
            SaveToFile();
        }
    }
}
