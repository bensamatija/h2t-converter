﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.VisualStyles;
using Microsoft.Office.Interop.Excel;
using TextFileRead1.Scripts;

namespace TextFileRead1
{
    public class MainProgram
    {
        //MainProgram mainProgram = new MainProgram();
        //private StringBuilder sb2 = new StringBuilder();
        //private StringBuilder sb1 = new StringBuilder();

        
        private Form1 form1 = new Form1();
        private RichTextBox richTextBox = new RichTextBox();

        public List<string> list_OfAllLines = new List<string>();
        public List<string> list_VenstreFodpunkt = new List<string>();
        public List<string> list_VenstreRammehjorne = new List<string>();
        public List<string> list_VenstreKipsamling = new List<string>();

        public string halberFrameFile_path;
        public string _richTextBox1;
        public string rawString;

        public int arraySelector = 0;

        public string filename_path;
        public string filename_file;
        public string filename_ext;

        /*
        // Auto-implemented properties.
        public int Age { get; set; }
        public string Name { get; set; }

        public class FilterID_1
        {
            // Properties:
            public string Name { get; set; }
            public int Age { get; set; }
        }
        */

        public void SelectFile()
        {
            var form1 = new Form1();
            var halberFrameFile = new OpenFileDialog();
            //halberFrameFile.InitialDirectory = "D:\\";
            halberFrameFile.Filter = ".TXT files (*.TXT)|*.TXT"; //|All files (*.*)|*.*";
            halberFrameFile.FilterIndex = 1;
            halberFrameFile.RestoreDirectory = true;
            if (halberFrameFile.ShowDialog() == DialogResult.OK)
            {
                halberFrameFile_path = halberFrameFile.FileName;
                
                filename_path = Path.GetDirectoryName(halberFrameFile_path);
                filename_file = Path.GetFileNameWithoutExtension(halberFrameFile_path);
                filename_ext = Path.GetExtension(halberFrameFile_path);
                
                
            }
            else
            {
                halberFrameFile.Dispose();
            }
        }

        public void ReadWholeFile()
        {
            try
            {
                // TODO Ckeck if null

                using (var sr = new StreamReader(halberFrameFile_path))
                {
                    while (sr.Peek() >= 0)
                    {
                        //  Separating the connections:
                        string thisLine = sr.ReadLine();

                        if (thisLine.Contains("Venstre fodpunkt"))
                        { arraySelector = 1; }
                        if (thisLine.Contains("Venstre rammehj"))
                        { arraySelector = 2; }
                        if (thisLine.Contains("Venstre kipsamling"))
                        { arraySelector = 3; }

                        if (arraySelector == 1)
                        { list_VenstreFodpunkt.Add(thisLine); }
                        if (arraySelector == 2)
                        { list_VenstreRammehjorne.Add(thisLine); }
                        if (arraySelector == 3)
                        { list_VenstreKipsamling.Add(thisLine); }

                        //list_OfAllLines.Add(sr.ReadLine());
                        list_OfAllLines.Add(thisLine);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR\n-> " + ex.Data + "\n->" + ex.Source + "\n->" + ex.TargetSite + "\n->" +
                                ex.Message + "\n->" + ex.InnerException + "\n->" + ex.StackTrace);
            }
        }

        public void CopyListToTextBox_A(RichTextBox _richTextBox1)
        {
            //Form1 _form1 = new Form1();
            try
            {
                for (int i = 0; i < list_OfAllLines.Count; i++)
                {
                    _richTextBox1.AppendText(list_OfAllLines[i] + "\n");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR\n-> " + ex.Data + "\n->" + ex.Source + "\n->" + ex.TargetSite + "\n->" +
                                ex.Message + "\n->" + ex.InnerException + "\n->" + ex.StackTrace);
            }
        }

        public void SearchForMatches(RichTextBox _richTextBoxDEBUG, RichTextBox _richTextBoxOut1)
        {
            FilterIDsArrayListProgram FilterIDsArrayListProgram = new FilterIDsArrayListProgram();

            /*for (int i = 0; i < list_OfAllLines.Count; i++)
            {
                FilterIDsArrayListProgram.GoThroughIDs(list_OfAllLines[i], _richTextBoxDEBUG, _richTextBoxOut1);
            }*/

            for (int i = 0; i < list_VenstreFodpunkt.Count; i++)
            { FilterIDsArrayListProgram.GoThroughIDs(list_VenstreFodpunkt[i], _richTextBoxDEBUG, _richTextBoxOut1); }
            for (int i = 0; i < list_VenstreRammehjorne.Count; i++)
            { FilterIDsArrayListProgram.GoThroughIDs(list_VenstreRammehjorne[i], _richTextBoxDEBUG, _richTextBoxOut1); }
            for (int i = 0; i < list_VenstreKipsamling.Count; i++)
            { FilterIDsArrayListProgram.GoThroughIDs(list_VenstreKipsamling[i], _richTextBoxDEBUG, _richTextBoxOut1); }
        }

        public void Test1()
        {

        }

        //-------------------------------------------------
        public void MyArray()
        {
            ArrayList myArray = new ArrayList(2);
            myArray.Add("One");
            myArray.Add("Two");
            myArray.Add("Three");
            myArray.Add("Four");
            myArray.Add("Five");
            myArray.Add("Six");

            myArray.TrimToSize();
            //Debug.WriteLine(myArray.Capacity);
            //Debug.WriteLine(myArray.Count);

            foreach (string s in myArray)
            {
                //Debug.WriteLine(s);
            }
        }
    }
}
