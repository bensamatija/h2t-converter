﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace TextFileRead1
{
    internal class GableFrame
    {
        public string richTextBox1_temp;
        public string richTextBoxOut1_temp;

        public string s, s1;
        public int i;

        private string capturedLine;
        private int currentLinePosition = 0;
        public string DS_gable = "DSGableFrame_attributes.";
        public string attributePosition;
        public string quotationMark;
        public bool formatGableFrame;
        public bool formatBuildUp;
        public bool UniversalConverter;
        public bool UniversalWriter;
        public bool Multi2;
        public bool Plus10;
        public bool Plus20;
        public bool QuotationMark;
        public bool profileWidth;
        public bool profileThickness;
        public int substringNum;
        public int substringLength;
        public int removeNum;
        private StringBuilder sb2 = new StringBuilder();
        private StringBuilder sb1 = new StringBuilder();

        public void readGableFrameMain()
        {
            try
            {
                var halberFrameFile = new OpenFileDialog();
                //halberFrameFile.InitialDirectory = "D:\\";
                halberFrameFile.Filter = ".TXT files (*.TXT)|*.TXT";//|All files (*.*)|*.*";
                halberFrameFile.FilterIndex = 1;
                halberFrameFile.RestoreDirectory = true;
                if (halberFrameFile.ShowDialog() == DialogResult.OK)
                {
                    var gableFrame = new GableFrame();

                    var halberFrameFile_path = halberFrameFile.FileName;

                    // This just copies captured data to textbox:
                    using (var sr = new StreamReader(halberFrameFile_path))
                    {
                        while (sr.Peek() >= 0)
                        {
                            sb2.AppendLine(sr.ReadLine());
                            //currentLinePosition++;

                            // Automatic detction of the frame:
                            //capturedLine = sr.ReadLine();     // skips every 2nd line !!!
                            if ((capturedLine != null) && (capturedLine.Contains("Profil ................")) && (capturedLine.Contains("x")))
                            {
                                formatBuildUp = true;
                            }
                            if ((capturedLine != null) && (capturedLine.Contains("Profil ................")) && !(capturedLine.Contains("x")))
                            {
                                formatGableFrame = true;
                            }
                        }
                    }
                    richTextBox1_temp = sb2.ToString();

                    // This converts text to correct format:
                    using (var sr = new StreamReader(halberFrameFile_path))
                    {
                        while (sr.Peek() >= 0)
                        {
                            //sb1.AppendLine(sr.ReadLine());             // This reads all data line by line
                            capturedLine = sr.ReadLine();
                            currentLinePosition++;
                            //Debug.WriteLine(capturedLine);

                            // KIPPLADE:
                            if (capturedLine.Equals("Venstre kipsamling"))
                            {
                                var kipplade = true;
                                while (kipplade)
                                {
                                    capturedLine = sr.ReadLine();
                                    Debug.WriteLine(capturedLine.ToString());

                                    /// FILTERS here ...
                                    if (capturedLine.StartsWith("Bolteafstand til c/kropplade")) { attributePosition = "B3DY "; substringNum = 44; substringLength = 7; UniversalWriter = true; Multi2 = true; QuotationMark = true; goto CallWriter; }
                                    if (capturedLine.StartsWith("Bolteafstand, c1")) { attributePosition = "B3OUT "; substringNum = 44; substringLength = 7; UniversalWriter = true; Plus10 = true; QuotationMark = false; goto CallWriter; }
                                    if (capturedLine.StartsWith("Karakt. flydesp�nd., tv�rplade ")) { attributePosition = "M9 "; substringNum = 44; substringLength = 10; UniversalWriter = true; Multi2 = false; QuotationMark = false; goto CallWriter; }
                                    if (capturedLine.StartsWith("Bolte ")) { attributePosition = "B3SIZE "; substringNum = 7; substringLength = 2; UniversalWriter = true; Multi2 = false; QuotationMark = false; goto CallWriter; }

                                    if (capturedLine.StartsWith("Bredde af tv�rplade"))
                                    { UniversalConverter = true; profileWidth = true; substringNum = 44; substringLength = 7; goto CallWriter; }

                                    if (capturedLine.Contains("r-PL")) // Tv�r-PL15
                                    { UniversalConverter = true; profileThickness = true; substringNum = 7; substringLength = 2; goto CallWriter; }

                                    /// SPECIAL FILTERS here ...
                                    if (profileWidth && profileThickness)
                                    {
                                    }

                                    // Count lines:
                                    currentLinePosition++;

                                // Call the code to WRITE data:
                                CallWriter:
                                    this.Writer(capturedLine, attributePosition, substringNum, substringLength, removeNum, UniversalConverter, UniversalWriter, Multi2, QuotationMark, profileWidth, profileThickness);

                                    #region
                                    // This throws NullReference:
                                    //GableFrame gableFrame_class = new GableFrame();
                                    //gableFrame_class.Writer(capturedLine, attributePosition, substringNum, removeNum, UniversalWriter, Multi2, QuotationMark);
                                    /*
                                    if (UniversalWriter)
                                    {
                                        // ### UNIVERSAL WRITER FOR ALL ATTRIBUTES ###
                                        s1 = (capturedLine.Substring(substringNum).Remove(removeNum).Replace(" ", string.Empty)).ToString();
                                        Int32.TryParse(s1, out i1);
                                        if (Multi2)
                                        {
                                            i = i1 * 2;
                                        }
                                        if (Plus10)
                                        {
                                            i = i1 + 10;
                                        }
                                        if (Plus20)
                                        {
                                            i = i1 + 2;
                                        }
                                        if (QuotationMark)
                                        {
                                            quotationMark = ('"').ToString();
                                        }
                                        if (!QuotationMark)
                                        {
                                            quotationMark = null;
                                        }
                                        s = sb1.AppendLine(DS_gable + attributePosition + quotationMark + i + quotationMark).ToString();
                                        richTextBoxOut1_temp = s.ToString();

                                        substringNum = 0;
                                        removeNum = 0;
                                        attributePosition = "XXX";
                                        UniversalWriter = false;
                                        Multi2 = false;
                                        Plus10 = false;
                                        Plus20 = false;
                                        QuotationMark = false;
                                        // ###########################################
                                    }*/
                                    #endregion

                                    // END of KIPPLADE:
                                    if (capturedLine.StartsWith("     �����������������������������������������������E  ===========")) //"�����������������������������������������������   ==========="   // "     ����������������������������������"
                                    {
                                        kipplade = false;
                                        //Debug.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END of KIPPLADE");
                                    }
                                }
                            }
                        }
                    }
                    //richTextBoxOut1_temp = sb1.ToString();
                }
            }
            //catch (Exception ex) { }
            catch (Exception ex) { MessageBox.Show("Error has occured! " + ex.Data + ex.Source + ex.TargetSite + ex.Message + ex.InnerException + ex.StackTrace); }
        }

        // Testing:
        private string StringA { get; set; }

        public void WriteLine(object sender)
        {
            StringA = "help";
        }

        public void Databaste(object sender)
        {
            var b = "I need ";
            var c = b + StringA;
        }

        public void Writer(string capturedLine, string attributePosition, int substringNum, int substringLength, int removeNum, bool UniversalConverter, bool UniversalWriter, bool Multi2, bool QuotationMark, bool profileWidth, bool profileThickness)
        {
            if (this.UniversalConverter)
            {
                s1 = (capturedLine.Substring(substringNum, substringLength).Replace(" ", string.Empty)).ToString();
                if (profileWidth)
                {
                }
                if (profileThickness)
                {
                    // CANT GO HERE ??!!
                }
            }

            if (this.UniversalWriter)
            {
                // ### UNIVERSAL WRITER FOR ALL ATTRIBUTES ###
                //s1 = (capturedLine.Substring(substringNum).Remove(removeNum).Replace(" ", string.Empty)).ToString();
                s1 = (capturedLine.Substring(substringNum, substringLength).Replace(" ", string.Empty)).ToString();

                int.TryParse(s1, out i);  //i1
                if (Multi2)
                {
                    i = i * 2;
                }
                if (Plus10)
                {
                    i = i + 10;
                }
                if (Plus20)
                {
                    i = i + 2;
                }
                if (QuotationMark)
                {
                    quotationMark = ('"').ToString();
                }
                if (!QuotationMark)
                {
                    quotationMark = null;
                }

                s = sb1.AppendLine(DS_gable + attributePosition + quotationMark + i + quotationMark).ToString();
                this.richTextBoxOut1_temp = s.ToString();
            }
            // ###############################
            // Reset Valuse:
            substringNum = 0;
            substringLength = 0;
            removeNum = 0;
            attributePosition = "XXX";
            this.UniversalWriter = false;
            this.UniversalConverter = false;
            Multi2 = false;
            Plus10 = false;
            Plus20 = false;
            QuotationMark = false;
            profileWidth = false;
            profileThickness = false;
            // ###############################
        }
    }//class GableFrame

    internal class GableFrame_KipPlade
    {
        // NOT USED
    }
}