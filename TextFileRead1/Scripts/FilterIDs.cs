﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Microsoft.Vbe.Interop;

using TextFileRead1.Scripts;
//using TextFileRead1;

namespace TextFileRead1.Scripts
{
    class FilterID_1
    {
        public int Id { get; set; }
        public string AttributeName { get; set; }
        public string SearchIndex { get; set; }
        public bool Final { get; set; }
        public bool QuotationMark { get; set; }
        public int SubstringStart { get; set; }
        public int SubstringLength { get; set; }
        public bool Multi_2 { get; set; }
        public bool Plus_10 { get; set; }
        public bool Plus_20 { get; set; }
        public int DataType { get; set; }
        //#################### 1 ############# 2 ############## 3 ############# 4 ########### 5 ########## 6 ################ 7 ################### 8 ############# 9 ################## 10 #####
        public FilterID_1(int dataType, bool final, bool quotationMark, bool multi_2, bool plus_10, bool plus_20, string attributeName, int substringStart, int substringLength, string searchIndex)
        {
            //Id = id;
            AttributeName = attributeName;
            Final = final;
            QuotationMark = quotationMark;
            SubstringStart = substringStart;
            SubstringLength = substringLength;
            SearchIndex = searchIndex;
            Multi_2 = multi_2;
            Plus_10 = plus_10;
            Plus_20 = plus_20;
            DataType = dataType;
        }
    }
    //// RULES: if dataType =
    // 1: Normal, 
    // 2: MaterialQuality, 
    // 3: plateDimesion_part1 (FLxxx), 
    // 4: plateDimension_part2 (*xx), 
    // 5: profileHeigth,
    // 6: c1
    // 7: c2

    class FilterIDsArrayListProgram
    {
        List<string> list_convertedAttributes = new List<string>();
        List<string> list_plateDimensions = new List<string>();
        List<int> list_plateLength = new List<int>();
        //ArrayList array_plateLength = new ArrayList();
        List<string> list_mainProfiles = new List<string>();

        public string RAW_LINE_s_Previous = "";

        public int connectionTypeAndPosition;
        public int subPosition1;

        public void GoThroughIDs(string _list_OfAllLines, RichTextBox _richTextBoxDEBUG, RichTextBox _richTextBoxOut1)
        {
            ArrayList arrayFilterID_1 = new ArrayList(2);
            //ArrayList arrayFilterID_2 = new ArrayList(2);
            //ArrayList arrayFilterID_3 = new ArrayList(2);

            ArrayList arrayMainProfiles = new ArrayList(2);

            // Connection Types:
            arrayFilterID_1.Add(new FilterID_1(100, false, false, false, false, false, "Venstre fodpunkt", 0, 30, "Venstre fodpunkt"));
            arrayFilterID_1.Add(new FilterID_1(200, false, false, false, false, false, "Venstre rammehj", 0, 30, "Venstre rammehj"));
            arrayFilterID_1.Add(new FilterID_1(300, false, false, false, false, false, "Venstre kipsamling", 0, 30, "Venstre kipsamling"));

            //---Foot Connection---//---Venstre fodpunkt---// [DET C 1]
            /// filters for foot connecton
            if (connectionTypeAndPosition == 10)
            {
                arrayFilterID_1.Add(new FilterID_1(1, true, true, true, false, false, "CHY", 44, 7, "Bolteafstand til c/kropplade")); // May need to do (plateDimesion_part1) - (2 * Bolteafstand til c/kropplade)
                //arrayFilterID_1.Add(new FilterID_1(6, true, false, false, true, false, "B4OUT", 44, 7, "Bolteafstand, c1"));
                arrayFilterID_1.Add(new FilterID_1(6, true, false, false, true, false, "CHOUT", 44, 7, "Bolteafstand til yderside"));
                arrayFilterID_1.Add(new FilterID_1(7, true, false, false, true, false, "rawC2", 44, 7, "Bolteafstand, c2")); // Don't need raw value of this
                //arrayFilterID_1.Add(new FilterID_1(2, true, true, false, false, false, "M9", 44, 10, "Karakt. flydesp�nd., tv�rplade"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "CHSIZE", 7, 2, "Bolte M"));
                arrayFilterID_1.Add(new FilterID_1(3, false, true, false, false, false, "S5", 44, 7, "Bredde af"));//Bredde af tv�rplade
                arrayFilterID_1.Add(new FilterID_1(4, true, true, false, false, false, "P8", 7, 2, "-PL"));//Tv�r-PL
                arrayFilterID_1.Add(new FilterID_1(1, false, false, false, false, false, "rawBoltQuolity", 44, 9, "Boltekvalitet")); // ??
                //P9LEN = ??
                //arrayFilterID_1.Add(new FilterID_1(5, false, false, false, false, false, "rawProfilHeigth", 44, 5, "Profilh"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "CWS1A", 54, 7, "Yderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "CWS1B", 54, 7, "Yderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "CWS2A", 54, 7, "Krop"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "CWS2B", 54, 7, "Krop"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "CWS3A", 54, 7, "Inderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "CWS3B", 54, 7, "Inderflange"));
            }

            //---Corner Connection---//---Venstre rammehjorne---// [DET H]
            if (connectionTypeAndPosition == 20)
            {
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "B4OUT", 44, 7, "Bolteafstand, c-y"));
                //arrayFilterID_1.Add(new FilterID_1(8, false, false, false, false, false, "c-i", 44, 7, "Bolteafstand, c-i"));     // M je 'nemogoce' dobiti

                arrayFilterID_1.Add(new FilterID_1(9, false, true, false, false, false, "BenProfile", 0, 8, "Ben"));   // DEL
                arrayFilterID_1.Add(new FilterID_1(9, false, true, false, false, false, "RigleProfile", 0, 8, "Rig"));   // DEL  
                arrayFilterID_1.Add(new FilterID_1(9, false, true, false, false, false, "P10_OR_P11", 44, 7, "     Profil ...")); //     Profil ........................... :                           
                //arrayFilterID_1.Add(new FilterID_1(9, false, true, false, true, false, "MainProfilesReady", 5, 10, "     Afstivningsplade"));

                arrayFilterID_1.Add(new FilterID_1(1, true, true, true, false, false, "B4DY", 44, 7, "Bolteafstand til c/kropplade")); // May need to do (plateDimesion_part1) - (2 * Bolteafstand til c/kropplade)
                arrayFilterID_1.Add(new FilterID_1(6, true, false, false, true, false, "B4OUT", 44, 7, "Bolteafstand, c1"));
                arrayFilterID_1.Add(new FilterID_1(7, true, false, false, true, false, "rawC2", 44, 7, "Bolteafstand, c2")); // Don't need raw value of this
                //arrayFilterID_1.Add(new FilterID_1(2, true, true, false, false, false, "M9", 44, 10, "Karakt. flydesp�nd., tv�rplade"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "B4SIZE", 7, 2, "Bolte M"));
                arrayFilterID_1.Add(new FilterID_1(3, false, true, false, false, false, "S5", 44, 7, "Bredde af"));//Bredde af tv�rplade
                arrayFilterID_1.Add(new FilterID_1(4, true, true, false, false, false, "P14", 7, 2, "-PL"));//Tv�r-PL
                arrayFilterID_1.Add(new FilterID_1(1, false, false, false, false, false, "rawBoltQuolity", 44, 9, "Boltekvalitet")); // ??
                //P9LEN = ??
                //arrayFilterID_1.Add(new FilterID_1(5, false, false, false, false, false, "rawProfilHeigth", 44, 5, "Profilh"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "HWS1A", 54, 7, "Yderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "HWS1B", 54, 7, "Yderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "HWS2A", 54, 7, "Krop"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "HWS2B", 54, 7, "Krop"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "HWS3A", 54, 7, "Inderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "HWS3B", 54, 7, "Inderflange"));
            }

            //---Kip Connection---//---Venstre kipsamling---// [DET F]           
            if (connectionTypeAndPosition == 30)
            {
                arrayFilterID_1.Add(new FilterID_1(3, false, true, true, false, false, "S5", 44, 7, "Profil......."));
                arrayFilterID_1.Add(new FilterID_1(1, true, true, true, false, false, "B3DY", 44, 7, "Bolteafstand til c/kropplade")); // May need to do (plateDimesion_part1) - (2 * Bolteafstand til c/kropplade)
                arrayFilterID_1.Add(new FilterID_1(6, true, false, false, true, false, "B3OUT", 44, 7, "Bolteafstand, c1"));
                arrayFilterID_1.Add(new FilterID_1(7, true, false, false, true, false, "rawC2", 44, 7, "Bolteafstand, c2")); // Don't need raw value of this
                arrayFilterID_1.Add(new FilterID_1(2, true, true, false, false, false, "M9", 44, 10, "Karakt. flydesp�nd., tv�rplade"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "B3SIZE", 7, 2, "Bolte M"));
                arrayFilterID_1.Add(new FilterID_1(3, false, true, false, false, false, "S5", 44, 7, "Bredde af"));//Bredde af tv�rplade
                arrayFilterID_1.Add(new FilterID_1(4, true, true, false, false, false, "P9", 7, 2, "-PL"));//Tv�r-PL
                arrayFilterID_1.Add(new FilterID_1(1, false, false, false, false, false, "rawBoltQuolity", 44, 9, "Boltekvalitet")); // ??
                                                                                                                                     //P9LEN = ??
                arrayFilterID_1.Add(new FilterID_1(5, false, false, false, false, false, "rawProfilHeigth", 44, 5, "Profilh"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "FWS1A", 54, 7, "Yderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "FWS1B", 54, 7, "Yderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "FWS2A", 54, 7, "Krop"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "FWS2B", 54, 7, "Krop"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "FWS3A", 54, 7, "Inderflange"));
                arrayFilterID_1.Add(new FilterID_1(1, true, false, false, false, false, "FWS3B", 54, 7, "Inderflange"));

            }

            arrayFilterID_1.TrimToSize();

            foreach (FilterID_1 _FilterID_1 in arrayFilterID_1)
            {

                if (_list_OfAllLines.Contains("Venstre fodpunkt"))
                { connectionTypeAndPosition = 10; }
                if (_list_OfAllLines.Contains("Venstre rammehj"))
                { connectionTypeAndPosition = 20; }
                if (_list_OfAllLines.Contains("Venstre kipsamling"))
                { connectionTypeAndPosition = 30; }


                //if (_list_OfAllLines.StartsWith(_FilterID_1.SearchIndex))
                if (_list_OfAllLines.Contains(_FilterID_1.SearchIndex))
                {
                    //*** Definiraj in nato tukaj loči 3 kategorije.

                    _richTextBoxDEBUG.AppendText(_FilterID_1.SearchIndex + "\n");
                    ConvertThisToAttribute(_FilterID_1, _list_OfAllLines, _richTextBoxOut1);
                }
            }
            /*
            foreach (FilterID_1 _FilterID_3 in arrayFilterID_3)
            {
                if (_list_OfAllLines.Contains(_FilterID_3.SearchIndex))
                {
                    _richTextBoxDEBUG.AppendText(_FilterID_3.SearchIndex + "\n");
                    ConvertThisToAttribute(_FilterID_3, _list_OfAllLines, _richTextBoxOut1);
                }
            }*/
        }

        public void ConvertThisToAttribute(FilterID_1 _FilterID_1, string _list_OfAllLines, RichTextBox _richTextBoxOut1)
        {
            string DS_GableFrame = "DSGableFrame_attributes.";
            string DS_WeldedFrame = "XXX";
            string RAW_LINE_s = "";
            
            string linePrefix = "";
            //string RAW_LINE_2_s = "";
            int RAW_LINE_i;
            //int i;
            //string multi = "";
            string qu = "";
            string spc = " ";
            //string special = "";

            // Only for debugging purposes:
            if (_list_OfAllLines.Contains("Venstre"))
            {
                linePrefix = "###===";
                DS_GableFrame = "";
                goto WRITE_THE_LINE;
                //return;
            }
            /*
            if (_list_OfAllLines.Contains("Ben"))
            {
                subPosition1 = 1;       //Ben
                goto WRITE_THE_LINE;
            }
            if (_list_OfAllLines.Contains("Rig"))
            {
                subPosition1 = 2;       //Ben
                goto WRITE_THE_LINE;
            }
            */

            RAW_LINE_s = (_list_OfAllLines.Substring(_FilterID_1.SubstringStart, _FilterID_1.SubstringLength).Replace(" ", string.Empty));



            if (_FilterID_1.QuotationMark) { qu = ('"').ToString(); }
            if (!_FilterID_1.QuotationMark) { qu = (""); }

            if (_FilterID_1.DataType == 1)
            {
                if (connectionTypeAndPosition == 10)
                {
                    //TODO Add Tolerance 2 or 3 mm as needed:

                }
            }

            if (_FilterID_1.DataType == 2)
            {
                if (RAW_LINE_s.Equals("275")) { RAW_LINE_s = RAW_LINE_s + "JR"; }
                if (RAW_LINE_s.Equals("355")) { RAW_LINE_s = RAW_LINE_s + "J2"; ; }
            }

            if (_FilterID_1.DataType == 3)
            {
                list_plateDimensions.Insert(0, "FL");
                list_plateDimensions.Insert(1, RAW_LINE_s);
                return;
            }
            if (_FilterID_1.DataType == 4)
            {
                list_plateDimensions.Insert(2, "*");
                list_plateDimensions.Insert(3, RAW_LINE_s);
                RAW_LINE_s = "";    //This is the last part of plateDimensions, should be found later in .txt:
                for (int i = 0; i < list_plateDimensions.Count; i++)
                {
                    RAW_LINE_s = RAW_LINE_s + list_plateDimensions[i];

                }
                list_plateDimensions.Clear();   // Because it is the last part, we can clear it now.
            }
            if (_FilterID_1.DataType == 5)
            {
                int.TryParse(RAW_LINE_s, out RAW_LINE_i);
                list_plateLength.Add(RAW_LINE_i);
                if ((list_plateLength.Count() == 3))
                {
                    RAW_LINE_s = (list_plateLength[2] - (list_plateLength[0] + list_plateLength[1])).ToString();
                    _FilterID_1.AttributeName = "B3DX"; //B3DX = Profilhřjde i střd - (c1 + c2) -> 240 - (55 + 43)
                    list_plateLength.Clear();
                }
            }
            if (_FilterID_1.DataType == 6)
            {
                int.TryParse(RAW_LINE_s, out RAW_LINE_i);
                list_plateLength.Add(RAW_LINE_i);
                return;
            }
            if (_FilterID_1.DataType == 7)
            {
                int.TryParse(RAW_LINE_s, out RAW_LINE_i);
                list_plateLength.Add(RAW_LINE_i);
                return;
            }
            if (_FilterID_1.DataType == 7)      // 7 OR 8 ???
            {
                int.TryParse(RAW_LINE_s, out RAW_LINE_i);
                RAW_LINE_s = (RAW_LINE_i - 20).ToString();
            }
            if (_FilterID_1.DataType == 9)
            {
                list_mainProfiles.Add(RAW_LINE_s);
                if (list_mainProfiles.Count == 2)
                {
                    if (list_mainProfiles[0].Contains("Ben"))
                    {
                        _FilterID_1.AttributeName = "P10";
                        RAW_LINE_s = list_mainProfiles[1];
                        _FilterID_1.Final = true;
                    }
                }
                if (list_mainProfiles.Count == 4)
                {
                    if (list_mainProfiles[2].Contains("Rig"))
                    {
                        _FilterID_1.AttributeName = "P11";
                        RAW_LINE_s = list_mainProfiles[3];
                        _FilterID_1.Final = true;
                    }
                }               
            }



            // At the end modify this:
            if (_FilterID_1.Multi_2) { int.TryParse(RAW_LINE_s, out RAW_LINE_i); RAW_LINE_i = RAW_LINE_i * 2; RAW_LINE_s = RAW_LINE_i.ToString(); }
            if (_FilterID_1.Plus_10)
            { int.TryParse(RAW_LINE_s, out RAW_LINE_i); RAW_LINE_i = RAW_LINE_i + 10; RAW_LINE_s = RAW_LINE_i.ToString(); }
            if (_FilterID_1.Plus_20) { int.TryParse(RAW_LINE_s, out RAW_LINE_i); RAW_LINE_i = RAW_LINE_i + 20; RAW_LINE_s = RAW_LINE_i.ToString(); }

            WRITE_THE_LINE:
            if (_FilterID_1.Final)
            {

                string LINE_CONVERTED = (linePrefix + DS_GableFrame + _FilterID_1.AttributeName + spc + qu + RAW_LINE_s + qu).ToString();
                //string LINE_CONVERTED = (DS_GableFrame + _FilterID_1.AttributeName + spc + qu + RAW_LINE_s + multi + RAW_LINE_2_s + special + qu).ToString();

                RAW_LINE_s_Previous = RAW_LINE_s;

                _richTextBoxOut1.AppendText(LINE_CONVERTED + "\n");
                list_convertedAttributes.Add(LINE_CONVERTED);
            }
        }
    }

}
